#!/bin/bash

KEYLAYOUT_CONFIG=${BACKED_UP_HOME}config/keyconfigs/layout/

ln -sf ${KEYLAYOUT_CONFIG}xmodmaprc-iso3 ~/.Xmodmaprc-hu
ln -sf ${KEYLAYOUT_CONFIG}xmodmaprc-iso3 ~/.Xmodmaprc-fr
ln -sf ${KEYLAYOUT_CONFIG}xmodmaprc-iso3 ~/.Xmodmaprc-de
ln -sf ${KEYLAYOUT_CONFIG}xmodmaprc-iso3 ~/.Xmodmaprc-es
ln -sf ${KEYLAYOUT_CONFIG}xmodmaprc ~/.Xmodmaprc
ln -sf ~/.Xmodmaprc ~/.Xmodmaprc-us
ln -sf ~/.Xmodmaprc ~/.Xmodmaprc-ru

mkdir -p ~/bin
ln -sf ${KEYLAYOUT_CONFIG}change-layout.sh ~/bin/change-layout.sh
ln -sf ${KEYLAYOUT_CONFIG}change-layout-to-fr-default.sh ~/bin/change-layout-to-fr-default.sh
ln -sf ${KEYLAYOUT_CONFIG}change-layout-to-fr.sh ~/bin/change-layout-to-fr.sh
ln -sf ${KEYLAYOUT_CONFIG}change-layout-to-hu.sh ~/bin/change-layout-to-hu.sh
ln -sf ${KEYLAYOUT_CONFIG}change-layout-to-ru.sh ~/bin/change-layout-to-ru.sh
ln -sf ${KEYLAYOUT_CONFIG}change-layout-to-us-default.sh ~/bin/change-layout-to-us-default.sh
ln -sf ${KEYLAYOUT_CONFIG}change-layout-to-us.sh ~/bin/change-layout-to-us.sh
ln -sf ${KEYLAYOUT_CONFIG}rofi_language-select.sh ~/bin/rofi_language-select.sh
