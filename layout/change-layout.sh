#!/bin/bash

KALNAR_KEY_LAYOUT=`setxkbmap -query | grep layout | cut -d ' ' -f 6`

case ${KALNAR_KEY_LAYOUT} in
  "us")
    KALNAR_KEY_LAYOUT=hu
    KALNAR_KEY_LAYOUT_LONG=hungarian
    ;;
  "hu")
    KALNAR_KEY_LAYOUT=fr
    KALNAR_KEY_LAYOUT_LONG=french
    ;;
  "fr")
    KALNAR_KEY_LAYOUT=ru
    KALNAR_KEY_LAYOUT_LONG=russian
    ;;
  "ru")
    KALNAR_KEY_LAYOUT=de
    KALNAR_KEY_LAYOUT_LONG=german
    ;;
  "de")
    KALNAR_KEY_LAYOUT=es
    KALNAR_KEY_LAYOUT_LONG=spanish
    ;;
  "es")
    KALNAR_KEY_LAYOUT=us
    KALNAR_KEY_LAYOUT_LONG=english
    ;;
esac

echo ${KALNAR_KEY_LAYOUT}
echo ${KALNAR_KEY_LAYOUT_LONG}

setxkbmap -layout ${KALNAR_KEY_LAYOUT}
notify-send -t 1000 'Layout change' "Changed to ${KALNAR_KEY_LAYOUT_LONG}"
xmodmap ~/.Xmodmaprc-${KALNAR_KEY_LAYOUT}
xmodmap ~/.Xmodmaprc-${KALNAR_KEY_LAYOUT}


