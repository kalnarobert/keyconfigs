#!/bin/bash

LANGUAGE=$(echo """spanish
german
russian
english
hungarian""" | rofi -dmenu)

case ${LANGUAGE} in
  "hungarian")
    KALNAR_KEY_LAYOUT=hu
    KALNAR_KEY_LAYOUT_LONG=hungarian
    ;;
  "french")
    KALNAR_KEY_LAYOUT=fr
    KALNAR_KEY_LAYOUT_LONG=french
    ;;
  "russian")
    KALNAR_KEY_LAYOUT=ru
    KALNAR_KEY_LAYOUT_LONG=russian
    ;;
  "german")
    KALNAR_KEY_LAYOUT=de
    KALNAR_KEY_LAYOUT_LONG=german
    ;;
  "spanish")
    KALNAR_KEY_LAYOUT=es
    KALNAR_KEY_LAYOUT_LONG=spanish
    ;;
  "english")
    KALNAR_KEY_LAYOUT=us
    KALNAR_KEY_LAYOUT_LONG=english
    ;;
esac

echo ${KALNAR_KEY_LAYOUT}
echo ${KALNAR_KEY_LAYOUT_LONG}

setxkbmap -layout ${KALNAR_KEY_LAYOUT}
notify-send -t 1000 'Layout change' "Changed to ${KALNAR_KEY_LAYOUT_LONG}"
xmodmap ~/.Xmodmaprc-${KALNAR_KEY_LAYOUT}
xmodmap ~/.Xmodmaprc-${KALNAR_KEY_LAYOUT}


